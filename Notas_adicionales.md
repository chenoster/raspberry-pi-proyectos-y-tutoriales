## Notas adicionales:

##### Actualización completa de raspiOS y firmware:  
`apt-get update`   
`apt-get upgrade`   
`rpi-update`  

##### Copia de seguridad de la tarjeta SD en linux, el tamaño del fichero será igual al tamaño total de la tarjeta así que después comprimimos:  
`dd if=/dev/xxxx of=nombre_fichero.img`  
`xz nombre_fichero.img`

(Borra el original, la tasa de compresión es muy alta pero tarda mucho)

Para descomprimir:  
`xz -d nombre_fichero.img.xz`

##### Ip pública y privada
· Ip privada: `ip a`  
· Ip pública: `curl ifconfig.me`

##### Compartir internet
https://rodrigo.zamoranelson.cl/2006/04/compartir-internet-con-linux-debian-y-derivados/

##### Programar un pulsador para que haga lo que queramos:
https://soloelectronicos.com/2018/10/18/como-agregar-un-boton-de-encendido-apagado-a-la-raspberry-pi/