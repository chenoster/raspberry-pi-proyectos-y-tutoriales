## duckDNS: Accede a tu rpi desde internet gracias a este servidor de DNS dinámico gratuito

[duckDNS](https://www.duckdns.org) es un servicio de DNS dinámico gratuito (hasta 5 subdominios .duckdns.org) seguro, privado y fácil de utilizar.

