## Instalación Rocket Chat en Raspberry Pi 4

1) Instalar Ubuntu Server 64 bits ([ver tutorial](https://gitlab.com/chenoster/raspberry-pi-proyectos-y-tutoriales/-/blob/master/instalar_ubuntu_server.md))

2) Actualizar el sistema operativo:   
`sudo apt-get update`  
`sudo apt-get upgrade`   
`sudo apt-get dist-upgrade`

3) Instalamos snap aunque normalmente viene instalado por defecto:  
`apt-get install snapd`  

4) Instalamos el servidor de rocket chat:  
`sudo snap install rocketchat-server`

5) Una vez instalado podemos entrar a través del navegador: `http://ip_rpi:3000`  

Hay que tener en cuenta que rocket chat es altamente flexible y configurable, muchas de sus funciones dependen de otros servicios,
por ejemplo, un servidor de correo para enviar las invitaciones, utiliza jitsi para las video conferencias, ...

Por tanto, la configuración puede resultar compleja y depende de los servicios y funcionalidad que queramos darle a nuestro servidor rocket.

Fuente: https://github.com/RocketChat/Rocket.Chat.RaspberryPi

Insta tu propio servidor Rocket Chat:
https://www.youtube.com/watch?v=EAxJS4cbW-E

Instalar tu propio servidor Jitsi (videoconferencias):
https://www.youtube.com/watch?v=cQOENjwFQB4
