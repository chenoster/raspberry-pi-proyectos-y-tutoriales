## Descarga la revista oficial de Raspberry Pi (MagPi)

Página oficial de la revista: https://magpi.raspberrypi.org

La revista se encuentra disponible de manera gratuita o también puedes suscribirte para recibirla en papel o a través de su aplicación para dispositivos móviles.

Estos sencillos scripts te facilitan la labor de descargar todos los números de la revista para que no tengas que descargarlos de uno en uno.

Hay 3 versiones:

· **Python:** Multiplataforma

· **Bash:** Para Linux

· **PowerShell:** Windows

Descarga el script que prefieras y editalo para cambiar la ruta donde deseas descargar la revista.


#### *Nota: La versión python requiere algunos módulos adicionales:

· requests

· lxml

· bs4

Los módulos se instalan con la orden *pip install*, por ejemplo: 

`pip install requests`


** Más pdf con ideas de proyectos en el siguiente enlace:
https://www.raspberrypi.org/magpi-issues/