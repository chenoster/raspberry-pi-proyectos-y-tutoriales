# ------------------------------------------------------------------
# [Author] @chenoster - https://gitlab.com/chenoster
#          Descarga todos los números de la revista MagPi.
#          Están disponibles de manera gratuita en: https://magpi.raspberrypi.org/issues/
#          o también puedes suscribirte y recibirlos en papel o a través de su aplicación móvil.
#          Este script está bajo licencia GNU GENERAL PUBLIC LICENSE
# ------------------------------------------------------------------

# VERSION=1.0
# USO="Descarga el script, modifica la ruta de descarga y ejecuta: python3 descarga_magpi.py"
import os
import requests
import lxml
from bs4 import BeautifulSoup

url_hasta_numero = 'https://magpi.raspberrypi.org/issues/' # Ejemplo url número 3 de la revista: https://magpi.raspberrypi.org/issues/03/pdf
url_final = '/pdf'
ruta_descarga = '/home/xxxxx/Documentos/' # Escribe aquí la ruta donde deseas guardar la revista, recuerda que el símbolo \ es el carácter de escape, así que en Windows la ruta se escribe 'C:\\Users\\xxxxx\\Downloads\\'

# Lista de revistas ya descargadas
lista_ficheros = os.listdir(ruta_descarga)

for numero_revista in range(1,100):
        nombre_fichero = 'MagPi' + str(numero_revista).zfill(2) + '.pdf'
        if not nombre_fichero in lista_ficheros:
                request = requests.get(url_hasta_numero + str(numero_revista).zfill(2) + url_final)
                if request.status_code == 200:
                        source = requests.get(url_hasta_numero + str(numero_revista).zfill(2) + url_final).text
                        soup = BeautifulSoup(source, 'lxml')
                        etiqueta_enlace = soup.find('a', class_='c-link')
                        enlace_revista = etiqueta_enlace.get("href")
                        print("Inicio descarga " + nombre_fichero)
                        fichero_pdf = requests.get(enlace_revista, allow_redirects=True)
                        open(ruta_descarga + nombre_fichero, 'wb').write(fichero_pdf.content)
                        print("Fin descarga " + nombre_fichero)