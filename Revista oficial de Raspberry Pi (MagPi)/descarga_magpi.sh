#!/bin/bash
# ------------------------------------------------------------------
# [Author] @chenoster - https://gitlab.com/chenoster
#          Descarga todos los números de la revista MagPi.
#          Están disponibles de manera gratuita en: https://magpi.raspberrypi.org/issues/
#          o también puedes suscribirte y recibirlos en papel o a través de su aplicación móvil.
#          Este script está bajo licencia GNU GENERAL PUBLIC LICENSE
# ------------------------------------------------------------------

# VERSION=1.0
# USO="Descarga el script, modifica la ruta de descarga, dale permisos de ejecución y ejecuta: ./descarga_magpi.sh"

url_hasta_numero="https://magpi.raspberrypi.org/issues/" # Ejemplo url número 3 de la revista: https://magpi.raspberrypi.org/issues/03/pdf
url_final="/pdf"
ruta_descarga="/home/xxxxx/Documentos/" # Escribe aquí la ruta donde deseas descargar la revista
n=""
nombre_fichero=""

# Listamos los ejemplares que ya tenemos descargados
ficheros=$(ls $ruta_descarga)

for numero_revista in {1..99}
do

    n=$(printf %01d$numero_revista)
    nombre_fichero="MagPi"$n".pdf"
    html=$ruta_descarga"MagPi"$n".html"

    if [[ ! ${ficheros[@]} =~ $nombre_fichero ]]; then

        enlace_revista=$url_hasta_numero$n$url_final
        wget -O $html $enlace_revista
        enlace_descarga=$(cat $html | grep c-link | sed -r 's/.*href="([^"]+).*/\1/g')
        wget -O $ruta_descarga$nombre_fichero $enlace_descarga
        rm $html

    fi

done