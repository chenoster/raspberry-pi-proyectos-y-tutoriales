# ------------------------------------------------------------------
# [Author] @chenoster - https://gitlab.com/chenoster
#          Descarga todos los números de la revista MagPi.
#          Están disponibles de manera gratuita en: https://magpi.raspberrypi.org/issues/
#          o también puedes suscribirte y recibirlos en papel o a través de su aplicación móvil.
#          Este script está bajo licencia GNU GENERAL PUBLIC LICENSE
# ------------------------------------------------------------------

# VERSION=1.0
# USO="Copia el texto del script, modifica la ruta donde deseas descargar las revistas y pegalo en la consola powershell
#      o guardalo en un archivo y ejecuta como script"

$url_hasta_numero = "https://magpi.raspberrypi.org/issues/" # Ejemplo url número 3 de la revista: https://magpi.raspberrypi.org/issues/03/pdf
$url_final = "/pdf"
$ruta_descarga = "C:\Users\xxxxx\Downloads\" # Escribe aquí la ruta donde deseas descargar la revista

# Listamos los ejemplares que ya tenemos descargados
$ficheros = Get-ChildItem -Path $ruta_descarga

for ($n=1; $n -lt 100; $n++){

    $numero_revista = $n.ToString().PadLeft(2,'0')
    $nombre_fichero = "MagPi" + $numero_revista + ".pdf"  
    $ruta_fichero = $ruta_descarga + $nombre_fichero
    $enlace_revista = $url_hasta_numero + $numero_revista + $url_final

    if(!(Test-Path $ruta_fichero)){
       
       $enlace_descarga = ((Invoke-WebRequest –Uri $enlace_revista).Links | Where class -eq "c-link").href 
       Invoke-WebRequest -Uri $enlace_descarga -OutFile $ruta_fichero

    }
}