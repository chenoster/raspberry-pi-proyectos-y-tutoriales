## Rpi zero como router avanzado con OpenWRT (bloqueo de publicidad, control parental, privacidad, ...)

#### AVISO!!!: FINALMENTE ABANDONO ESTA IDEA Y ME PASO A PI-HOLE. https://pi-hole.net/

La idea es conectar la rpi zero al ordenador a través del puerto USB que le proporciona alimentación y será la interface de red. A su vez la rpi estará conectada por wifi al punto de acceso de internet.
Por tanto todo el tráfico exterior pasará por nuestra rpi que actuará como filtro y puede también conectarse a una VPN o a la red TOR de manera transparente, sin tocar nada en nuestro equipo.

Todo lo que puedes hacer con este router personalizado:
~~~
· Configuración totalmente flexible y sistema opensource  
· Bloquear publicidad (con lo que también ahorras ancho de banda)  
· Control parental (bloquear sitios web de contenido inapropiado, horarios de conexión, ...)  
· Privacidad (navegar a través de VPN, utilizar la red TOR, ...)  
· Router de viaje para conectarse a redes públicas de manera segura  
· Y mucho más...  
~~~
Esto se puede hacer también con Raspbian + pi-hole y en general con cualquier sistema operativo pero openwrt ofrece una interface web y un sistema de plugings muy fácil de configurar.

1) Descargar el firmware: https://openwrt.org/toh/hwdata/raspberry_pi_foundation/raspberry_pi_zero_w

2) Descomprimir y copiar la imagen de disco en la tarjeta SD con algún software especializado como Balena-Etcher o con el comando dd

3) Primero vamos a introducir la tarjeta sd en la rpi zero, arrancamos y esperamos 5 minutos. En el primer inicio se crean unos ficheros de configuración que luego vamos a modificar.

4) Apagamos la rpi y volvemos a colocar la tarjeta en el ordenador para modificar unos ficheros.

5) Activar la WiFi. Para ello vamos a editar un fichero de configuración de la tarjeta SD que se encuentra en la segunda partición (la más grande).  
Vamos al fichero /etc/config/wireless, debe quedar algo así:
~~~
config wifi-device 'radio0'  
        option type 'mac80211'  
        option channel '11'             # Cambiar el canal si es necesario  
        option hwmode '11g'  
        option path 'platform/soc/20300000.mmc/mmc_host/mmc1/mmc1:0001/mmc1:0001:1'  
        option htmode 'HT20'  
        option disabled '0'            # 0 Activado  

config wifi-iface 'wlan0'              # Cambio el nombre de la interface  
        option device 'radio0'  
        option network 'lan'  
        option mode 'sta'               # ap punto de acceso, sta cliente  
        option ssid 'nombre_red_wifi'   # Nombre red wifi  
        option encryption 'psk2'        # WPA2  
        option key 'contraseña_wifi'    # Contraseña wifi  
~~~
** *Referencia configuración:* https://openwrt.org/docs/guide-user/network/wifi/basic

Y también modificamos el fichero /etc/config/network:
~~~
config interface 'loopback'  
        option ifname 'lo'  
        option proto 'static'  
        option ipaddr '127.0.0.1'  
        option netmask '255.0.0.0'  

config globals 'globals'  
        option ula_prefix 'fd84:ce30:f602::/48'  

config interface 'lan'  
        option ifname 'wlan0'  
        option proto 'dhcp'  
~~~
** *Referencia configuración:* https://openwrt.org/docs/guide-user/base-system/basic-networking

6) Colocamos la tarjeta en la rpi zero y arrancamos. Vamos a conectar por SSH. La ip la podemos encontrar en la configuración de nuestro router o con el comando nmap -sn <ip>.  El usuario root no tiene contraseña, habrá que cambiarla por seguridad.    

8) Vamos a lanzar los siguientes comandos:
~~~
opkg update  
opkg install kmod-usb-gadget-eth  
opkg install kmod-usb-dwc2  
halt  
~~~
Con esto actualizamos el sistema e instalamos los drivers para usar el puerto USB como interface ethernet, finalmente reiniciamos.

9) Volvemos a sacar la tarjeta sd para añadir una linea al final de config.txt, este fichero se encuentra en la primera partición: `dtoverlay=dwc2`

10) Editamos también el fichero cmdline.txt y añadimos `modules-load=dwc2,g_ether` justo después de la palabra *rootwait*

** *Referencia:* https://github.com/smeathers/lede-pi0w-usb

11) Accedemos a la interface web desde el navegador: http://<ip_raspberry>  
Recordemos que el usuario root no tiene contraseña, ahora es buen momento para poner una.

12) Desde aquí podemos crear la interface USB para compartir el acceso a internet con nuestro equipo.

13) Ahora todo el tráfico de red pasa por la rpi antes de llegar a nuestro ordenador, por lo que podemos instalar los módulos que consideremos necesarios dependiendo de para que lo vayamos a usar: conectar a una VPN, bloquear publicidad, ...
Para más información lo mejor es ir a la página oficial del proyecto OpenWrt: https://openwrt.org/start?id=es/start