## Instalación de Ubuntu Server en Raspberry pi 2, 3 ó 4

Hay versiones de 32 y 64 bits:  
https://ubuntu.com/download/raspberry-pi

Tutorial oficial de instalación:  
https://ubuntu.com/tutorials/how-to-install-ubuntu-on-your-raspberry-pi#1-overview

Descargar y quemar imagen en la tarjeta SD.

Conectamos al router por cable y buscamos la ip de la rpi:

sudo nmap -sn 192.168.1.0/24

Conectamos por ssh. Usuario y contraseña: ubuntu