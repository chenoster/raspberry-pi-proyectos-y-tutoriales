## MANUAL DE DOCKER: Comandos básicos

#### Permisos para el entorno de desarrollo
Para cualquier acción requiere privilegios de administración. Podemos añadir nuestro usuario al grupo docker para que esto no ocurra, lo cual facilita mucho la labor.

#### En Fedora 32 y posteriores. Solución error cgroups V2
`sudo dnf install -y grubby && sudo gruby --update-kernel=ALL --args="systemd.unified_cgroup_hierarchy=0"`  
`reboot`

#### Instalar Docker en Raspbian 
`sudo apt-get update && sudo apt-get upgrade`  
`curl -fsSL https://get.docker.com -o get-docker.sh`  
`sudo sh get-docker.sh`   
`sudo usermod -aG docker pi`  

Imágenes compatibles con Raspberry: https://registry.hub.docker.com/search?q=&type=image&architecture=arm%2Carm64

#### Iniciar servicio docker
`systemctl start docker`

#### Descargar una imagen de hub.docker
`docker <imagen:etiqueta>`

#### Listar imagenes
`docker images`

#### Listar contenedores, todos ó sólo los que están activos
`docker ps -a`  
`docker ps`

#### Borrar una imagen
`docker rmi <imagen>`

#### Borrar un contenedor
`docker rm <contenedor>`

#### Arrancar un contenedor por primera vez (run, configura y arranca)
`docker run` 

#### Arrancar un contenedor por primera vez (configura y arranca), le doy un nombre personalizado y entra de manera interactiva
`docker run -it --name <nombrepersonalizado> <imagen>`

#### Arrancar, reiniciar o parar un contenedor
`docker start <contenedor>`  
`docker start -it <contenedor>` (modo interactivo, consola)  
`docker start -i <contenedor>` (ver consola)  
`docker restart <contenedor>`  
`docker stop <contenedor>`  
`docker kill <contenedor>` (por si no responde)  

#### Entrar en la consola de un contenedor que está en ejecución
`docker exec -it <contenedor> bash`

#### Configuración de red: IP
Por defecto, a los contenedores que vamos levantando se les asigna una ip que va desde la 172.17.0.2 en adelante.

#### Ver IP y todas las propiedades de un contenedor
`docker inspect <contenedor> | grep IP`

#### Mapeo de puertos
`docker run -it --name <nombre_personalizado_contenedor> -p 8081:8080 <imagen>` (mapea el puerto 8081 al 8080 del contenedor)

#### Volúmenes: carpetas compartidas
`docker run -it --name <nombre_personalizado> -p 8081:8080 -v <ruta_equipo>:<ruta_contenedor> <imagen>`

#### Variables de entorno
`docker run --name mibasededatos -e MYSQL_ROOT_PASSWORD=xxxxxxx -d mariadb:tag` (-d lo lanza como servicio/demonio)

#### Ver logs. También se pueden ver en la terminal de visual code (con la extensión docker)
`docker logs -f <contenedor>`

#### Acceder al contenedor. Se puede hacer pero no es lo más recomendable, es mejor extender el contenedor
`docker exec -it <contenedor> bash`

#### Referencias entre contenedores
`docker run -it --name <nombre_personalizado> --link mibasedatos:mimariadb centos:centos7 /bin/bash`

#### Crear imágenes 
`FROM centos:7` (Imagen de la que partimos)

ADD https://ruta_fichero_en_internet /carpeta_destino_en_contenedor (Añadir ficheros, da más opciones, por ejemplo urls)
COPY /ruta_fichero_origen /ruta_carpeta_destino_del_contenedor (Añadir ficheros)
ADD /ruta_fichero_orien /docker-entrypoint-initdb.d/carga_datos.sql (Se podría cargar un script de base de datos al arrancar el contenedor)

`RUN echo "Hola, Mundo!"` (Ejecutar comandos)

`EXPOSE 8080` (Abrir o exponer puertos)

`ENTRYPOINT systemctl start apache2` (Arrancar programas dentro del contenedor, permite parametrizar en el arranque)
`CMD ["systemctl", "start", "apache2", "-u", "usuario"]` (Arrancar programas dentro del contenedor)

`VOLUME /var/logs/` (Exponer una carpeta del contenedor, luego habrá que decirle donde montar la carpeta) 

`USER usuario` (Cambio de usuario, de root que suele ser el usuario por defecto al usuario "usuario")

`ENV MYDATABASE_DEFAULT base_datos_por_defecto` (Definir variables de entorno)

`MAINTAINER chenoster` (Indica quién se encarga de mantener el contenedor)

`ONBUILD rm xxx` (Permite lanzar un comando que será lanzado cuando tu imagen sea utilizada para crear otra imagen)

`WORKDIR /home/xxx` (Indica cuál es el directorio de trabajo del contenedor)

#### Para construir la imagen vamos a la carpeta donde tenemos el Dockerfile
`docker build -t <etiqueta_personalizada> .`

#### Crear una imagen a partir de un contenedor
`docker commit <id_contenedor> <nombre_nueva_imagen>`

#### Exportar e importar un contenedor (para backup o distribución)
`docker export <contenedor id> > /home/usuario/export.tar`  
`cat /home/usuario/export.tar | docker import -nombre:etiqueta`

#### Docker-compose
Sirve para gestionar varias máquinas desde un sólo fichero similar a los Dockerfiles
