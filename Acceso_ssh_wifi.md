Una vez instalado el sistema operativo en la tarjeta microSD nos encontramos con que necesitamos un teclado y un monitor para acceder a nuestra rpi. Esto se puede evitar si activamos el servicio ssh y la conectamos a nuestra red. 

En este caso vamos a conectar por WiFi porque la rpi zero W no tiene puerto ethernet. Para ello, tenemos que crear un par de ficheros en la tarjeta microSD:

1) En la partición boot creamos un fichero vacío que se llame *ssh*
2) En la partición rootfs, vamos a la carpeta /etc/network/interfaces.d/ y creamos un fichero *wireless* con el siguiente contenido:
~~~
allow-hotplug wlan0  
iface wlan0 inet dhcp  
wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
~~~
3) En la particion rootfs, vamos a la carpeta /etc/wpa_supplicant y editamos el fichero wpa_supplicant.conf:
~~~
country=GB

network={  
    ssid="YOUR_NETWORK_NAME"  
    psk="YOUR_NETWORK_PASSWORD"  
    proto=RSN  
    key_mgmt=WPA-PSK  
    pairwise=CCMP  
    auth_alg=OPEN  
}
~~~