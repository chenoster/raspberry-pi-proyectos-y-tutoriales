#!/bin/sh
# -----------------------------------------------------------------------------------
# [Author] @chenoster - https://gitlab.com/chenoster
#          Instala Arch Linux en tu tarjeta SD y configura la WiFi
#          Para Raspberry Pi 4
#          Este script está bajo licencia GNU GENERAL PUBLIC LICENSE
# -----------------------------------------------------------------------------------

# VERSION=1.0
#
# USO: sudo ./arch_on_rpi4_wifi_write_sd.sh /dev/tu_tarjetaSD wifi_ssid wifi_password
#
# lsblk para ver dispositivos conectados (tarjetas, pendrives, discos) 
#
# Conectar por ssh:
# usuario alarm contraseña alarm
# usuario root contraseña root
#
# pacman-key --init
# pacman-key --populate archlinuxarm
# pacman -Syu

set -e # Salir inmediatamente si falla algún comando

if [[ $# -ne 3 ]] ; then
   echo "Uso: $0 </dev/disk> <ssid> <passphase>"
   exit 1
fi

DISK="$1"
SSID="$2"
PASS="$3"

if [[ ! -b "${DISK}" ]] ; then
   echo "No es un dispositivo de bloques: ${DISK}"
   exit 1
fi

if [[ "${USER}" != "root" ]] ; then
   echo "Debes ejecutarlo como root."
   exit 1
fi

echo "Creando particiones"

sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << EOF | fdisk ${DISK}
o # Limpia la tabla de particiones
n # Crea una nueva partición
p # Partición primaria
1 # Partición número 1
 # Por defecto, comienza al principio del disco 
+200M # Tamaño de la partición
t # Para determinar el tipo de partición
c # Tipo de partición, c corresponde a W95 FAT32 (LBA).
n # Creamos una nueva partición
p # Partición primaria
2 # Partición número 2
 # Por defecto, comienza inmediatamente después de la partición predecesora
 # Por defecto, extiende la partición hasta el final del disco
w # Escribe la tabla de particiones
q # Salimos de fdisk
EOF
 
if [[ "${DISK}" == *"mmcblk"* ]]; then
   DISK_P1=${DISK}p1
   DISK_P2=${DISK}p2
else 
   DISK_P1=${DISK}1
   DISK_P2=${DISK}2
fi
 
mkfs.vfat ${DISK_P1}
mkdir boot
mount ${DISK_P1} boot

mkfs.ext4 ${DISK_P2}
mkdir root
mount ${DISK_P2} root

echo "Copiando ficheros (esto puede tardar un rato)"

if [[ ! -f ArchLinuxARM-rpi-4-latest.tar.gz ]]; then
   wget http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-4-latest.tar.gz
fi

bsdtar -xpf ArchLinuxARM-rpi-latest.tar.gz -C root
sync

mv root/boot/* boot

echo "Activando WiFi"

cat << EOF >> root/etc/systemd/network/wlan0.network
[Match]
Name=wlan0

[Network]
DHCP=yes
EOF

wpa_passphrase "${SSID}" "${PASS}" > root/etc/wpa_supplicant/wpa_supplicant-wlan0.conf

ln -s \
   /usr/lib/systemd/system/wpa_supplicant@.service \
   root/etc/systemd/system/multi-user.target.wants/wpa_supplicant@wlan0.service

echo "Desmontando particiones"
umount root
umount boot

echo "Limpiando"
rmdir root
rmdir boot

echo "¡Hecho!"