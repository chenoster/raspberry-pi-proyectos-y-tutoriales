# Raspberry pi. Proyectos y tutoriales

¡Saca el máximo partido a tu Raspberry pi! Enlaces a los tutoriales:

· [Descarga todos los números de MagPi, la revista oficial de Raspberry Pi](https://gitlab.com/chenoster/raspberry-pi-proyectos-y-tutoriales/-/tree/master/Revista%20oficial%20de%20Raspberry%20Pi%20(MagPi))

· [Instala Raspberry Pi OS (antes llamado Raspbian), es el sistema operativo oficial para rpi basado en Debian](https://gitlab.com/chenoster/raspberry-pi-proyectos-y-tutoriales/-/blob/master/Preparar_tarjeta_sd_SO_oficial.md)

· [Accede a tu Raspberry Pi por ssh conectándola al puerto USB de tu ordenador](https://gitlab.com/chenoster/raspberry-pi-proyectos-y-tutoriales/-/blob/master/Acceso_ssh_por_USB.md)

· [Convierte cualquier dispositivo USB en inalámbrico (WiFi) con tu Raspberry Pi](https://gitlab.com/chenoster/raspberry-pi-proyectos-y-tutoriales/-/blob/master/Compartir_dispositivos_USB_en_red.md)

· [Script de instalación de Arch Linux en rpi Zero W con WiFi activa y configurada](https://gitlab.com/chenoster/raspberry-pi-proyectos-y-tutoriales/-/blob/master/arch_on_rpizero_wifi_write_sd.sh)

· [Script de instalación de Arch Linux en rpi 4 con WiFi activa y configurada](https://gitlab.com/chenoster/raspberry-pi-proyectos-y-tutoriales/-/blob/master/arch_on_rpi4_wifi_write_sd.sh)

· [Router avanzado con rpi zero y openwrt (bloqueo de publicidad, privacidad, control parental, ...)](https://gitlab.com/chenoster/raspberry-pi-proyectos-y-tutoriales/-/blob/master/Router_personalizado_openwrt_rpizero.md)

· [Apagado seguro: añade un botón de apagado/reinicio a tu raspberry pi](https://gitlab.com/chenoster/raspberry-pi-proyectos-y-tutoriales/-/blob/master/boton_apagado_seguro.md) 

· [Acceso SSH seguro sin tener que escribir la contraseña](https://gitlab.com/chenoster/raspberry-pi-proyectos-y-tutoriales/-/blob/master/ssh_sin_contrasena.md)

· [Conecta diodos LED directamente a tu rpi y controlalos mediante programación](https://gitlab.com/chenoster/raspberry-pi-proyectos-y-tutoriales/-/blob/master/rpi_led.md)

· [Crea un mando a distancia universal para todos tus aparatos](https://gitlab.com/chenoster/raspberry-pi-proyectos-y-tutoriales/-/blob/master/rpizero_mando_ir_universal.md)

· [Instalación de Ubuntu Server (Para Rpi 2, 3 y 4)](https://gitlab.com/chenoster/raspberry-pi-proyectos-y-tutoriales/-/blob/master/instalar_ubuntu_server.md)

· [Tu propio servidor de Rocket Chat sobre Raspberry Pi 4](https://gitlab.com/chenoster/raspberry-pi-proyectos-y-tutoriales/-/blob/master/instalacion_rocket_chat.md)

· [Instalar servidor Web LAMP (Linux, Apache, MariaDB y PHP) ](https://gitlab.com/chenoster/raspberry-pi-proyectos-y-tutoriales/-/blob/master/servidor_LAMP.md)

· [Instalación de Chamilo, plataforma de e-learning, buenísima alternativa a Moodle](https://gitlab.com/chenoster/raspberry-pi-proyectos-y-tutoriales/-/blob/master/instalacion_chamilo.md)

· [Tu Raspberry accesible desde internet gracias a duckDNS](https://gitlab.com/chenoster/raspberry-pi-proyectos-y-tutoriales/-/blob/master/duckDNS_rpi_accesible_desde_internet.md)

· [Manual de Docker: comandos básicos](https://gitlab.com/chenoster/raspberry-pi-proyectos-y-tutoriales/-/blob/master/manual_docker.md)

· [Servidor de correo en 2 minutos](https://gitlab.com/chenoster/linux-apuntes-y-utilidades/-/blob/master/servidor_correo_en2min.md)

· [Notas adicionales](https://gitlab.com/chenoster/raspberry-pi-proyectos-y-tutoriales/-/blob/master/Notas_adicionales.md)