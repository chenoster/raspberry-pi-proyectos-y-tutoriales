### Comparte cualquier dispositivo USB a través de tu red y utilízalo como si estuviera conectado localmente

Convierte cualquier dispositivo USB (disco duro, impresora, webcam, ...) en inalámbrico gracias a tu Raspberry pi.  

1) Instala usbip:
`sudo apt-get install usbip`

2) Activa el driver en el servidor:
`sudo modprobe usbip-host`
3) Lista los dispositivos conectados:
`sudo usbip list -l`

4) Enlaza el dispositivo, el id son dos números separados por - :
`sudo usbip bind --busid <busid>`

Debe aparecer un mensaje confirmando el enlace:
*bind device on busid <busid>: complete*

5) Por último, levantamos el servicio:
`sudo usbipd -D`

Para bajar el servicio: `sudo usbipd -D stop`  
Para desligar un dispositivo: `sudo usbip unbind --busid <busid>`

#### Ahora vamos a configurar el cliente (Linux):

1) Instala el paquete usbip

2) Activa el driver:
`sudo modprobe vhci-hcd`

3) Listamos los dispositivos remotos disponibles:
`sudo usbip list -r <ip raspberry>`

4) Enlazamos el dispositivo:
`sudo usbip attach -r <ip raspberry> -d <id dispositivo usb>`

Para ver dispositivos conectados: `sudo usbip port` (Nos fijamos en el número de puerto para luego desligar)  
Para desligar un dispositivo: `sudo usbip detach -p <puerto>`


** Existe una versión para Windows pero no puedo asegurar su funcionamiento porque no lo he probado.

*** Alternativa: [VirtualHere](https://github.com/klabarge/fob)

