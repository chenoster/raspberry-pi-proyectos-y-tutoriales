## Mando a distancia universal IR para todos tus aparatos 

En principio sólo vamos a necesitar el transmisor, porque en internet podemos encontrar los códigos de señal de casi todos los mandos comerciales (buscando en google por marca y modelo, p.e.: lirc "samsung A57-0023A").

Si no encontramos nuestro mando o no están todos los botones, necesitaremos también el receptor para grabar las señales. 

Emisor y receptor son partes totalmente independientes, vamos a ver primero la parte del emisor, ya que el receptor es opcional.

### Emisor

El emisor o transmisor de infrarrojos (IR) nos sirve para enviar señales a los diferentes aparatos, utilizando la rpi como mando a distancia. El montaje mínimo es extremadamente sencillo, consiste simplemente en conectar
un diodo directamente a la raspberry con la patilla larga (positivo) en un pin GPIO y la corta (negativo) conectada a un pin tierra (GROUND o GND en inglés).

En esta web se puede ver para qué sirve cada uno de los pines de la raspberry: https://es.pinout.xyz/#

Según fuentes de internet que se citarán al final del texto, con este montaje la señal será débil y tendrá poco alcance, por lo que se recomienda crear un pequeño circuito con un transistor para amplificar la señal.
A mi me ha resultado suficiente con el montaje mínimo pero también vamos a describir el montaje con amplificador.

![rpi zero convertido en mando IR](Imagenes/rpizero_mandoIR.png)

#### HARDWARE:

###### Mínimo:

- Raspberry pi
- Led IR emisor (suele ser un led transparente)

Se pueden conectar diodos led directamente a la rpi, la patilla larga (ánodo) va siempre al polo positivo, en este caso el polo positivo es un pin GPIO. La pata corta (cátodo) va a cualquier pin tierra (GROUNG o GND) que es el polo negativo.

Si tenemos un diodo con las patillas iguales porque lo hemos reciclado de algún aparato y se han recortado las patillas,nos fijamos en su interior: la parte más gruesa corresponde al polo negativo y el lado con la parte fina es el positivo.

En este caso vamos a conectar la patilla larga al pin GPIO22 (pin físico 15) y la patilla corta a tierra (en mi caso lo conecto al pin físico número 20).

###### Opcional: ------------------------------------------------------------------------------------------------------------------

- Transistor PN2222
- Resistencia 10K Ohm
- Placa de circuito
- Soldador de estaño

![Circuito IR](Imagenes/circuito_ir.png)
![Dibujo circuito amplificador IR](Imagenes/circuito_amplificador_ir.png)

Montamos el circuito tal y como aparece en la imagen. Si queremos poner también el receptor, aunque su funcionamiento es independiente, tendremos que dejar hueco para integrarlo en el circuito.

**¡Atención! Antes de soldar, conviene conectarlo en una placa de pruebas o unir los componentes provisionalmente y probar que todo funciona correctamente.**

Fin Opcional ------------------------------------------------------------------------------------------------------------------------

#### SOFTWARE

Ahora vamos a instalar y configurar el software necesario. El pin GPIO23 se incluye en la configuración por si en algún momento queremos conectar el receptor IR.

`sudo apt-get install lirc`

`sudo cp /etc/modules /etc/modules.bak`

~~~
sudo cat >> /etc/modules <<EOF
lirc_dev
lirc_rpi gpio_in_pin=23 gpio_out_pin=22
EOF
~~~

`sudo cp /etc/lirc/hardware.conf /etc/lirc/hardware.conf.bak`

~~~
sudo cat > /etc/lirc/hardware.conf <<EOF 
########################################################
# /etc/lirc/hardware.conf
#
# Arguments which will be used when launching lircd
LIRCD_ARGS="--uinput"
# Don't start lircmd even if there seems to be a good config file
# START_LIRCMD=false
# Don't start irexec, even if a good config file seems to exist.
# START_IREXEC=false
# Try to load appropriate kernel modules
LOAD_MODULES=true
# Run "lircd --driver=help" for a list of supported drivers.
DRIVER="default"
# usually /dev/lirc0 is the correct setting for systems using udev
DEVICE="/dev/lirc0"
MODULES="lirc_rpi"
# Default configuration files for your hardware if any
LIRCD_CONF=""
LIRCMD_CONF=""
######################################################## 
EOF
~~~

`cp /boot/config.txt /boot/config.txt.bak`

Para versiones del kernel igual o superiores a 4.19 (para ver versión usar comando uname -a):

~~~
cat >> /boot/config.txt <<EOF
dtoverlay=gpio-ir-tx,gpio_pin=22
dtoverlay=gpio-ir,gpio_pin=23
EOF 
~~~

Si el kernel es anterior a la versión 4.19 sería así:

~~~
cat >> /boot/config.txt <<EOF
dtoverlay=lirc-rpi,gpio_in_pin=23,gpio_out_pin=22
EOF 
~~~

Ahora vamos a buscar en internet la configuración de nuestro mando a distancia, buscando en google por marca y modelo, por ejemplo: lirc "samsung A57-0023A".

Buscamos un archivo .conf con un contenido similar a este:

~~~
# Please make this file available to others
# by sending it to <lirc@bartelmus.de>
#
# this config file was automatically generated
# using lirc-0.9.0-pre1(default) on Sun Jun  5 10:36:40 2011
#
# contributed by Alex 'AdUser' Z <ad_user@runbox.com>
#
# brand:             Samsung A57-0023A (sn on remote)
#                    Samsung LE32A345 (tv model)
# model no. of remote control: 
# devices being controlled by this remote:
#
  #toggle_bit_mask 0x1010

begin remote

  name  tv
  bits           16
  flags SPACE_ENC|CONST_LENGTH
  eps            30
  aeps          100

  header       4562  4516
  one           565  1687
  zero          565   560
  ptrail        564
  pre_data_bits   16
  pre_data       0xE0E0
  gap          108529
  toggle_bit_mask 0x0

      begin codes
          KEY_POWER                0x40BF # Power
          KEY_UNKNOWN              0xD12E # HDMI
          KEY_UNKNOWN              0x807F # Source
          KEY_1                    0x20DF
          KEY_2                    0xA05F
          KEY_3                    0x609F
          KEY_4                    0x10EF
          KEY_5                    0x906F
          KEY_6                    0x50AF
          KEY_7                    0x30CF
          KEY_8                    0xB04F
          KEY_9                    0x708F
          KEY_TEXT                 0x34CB # Teletext
          KEY_0                    0x8877
          KEY_UNKNOWN              0xC837 # Pre-Ch (Switch Channels)
          KEY_VOLUMEUP             0xE01F
          KEY_VOLUMEDOWN           0xD02F
          KEY_MUTE                 0xF00F
          KEY_CHANNEL              0xD629 # Channel List
          KEY_CHANNELUP            0x48B7
          KEY_CHANNELDOWN          0x08F7
          KEY_MEDIA                0x31CE
          KEY_UNKNOWN              0x58A7 # TV Menu
          KEY_UNKNOWN              0xF20D # Guide
          KEY_UNKNOWN              0xD22D # Tools
          KEY_INFO                 0xF807 # Info
          KEY_BACK                 0x1AE5 # Return
          KEY_EXIT                 0xB44B
          KEY_ENTER                0x16E9
          KEY_DOWN                 0x8679
          KEY_UP                   0x06F9
          KEY_LEFT                 0xA659
          KEY_RIGHT                0x46B9
          KEY_RED                  0x36C9 # A
          KEY_GREEN                0x28D7 # B
          KEY_YELLOW               0xA857 # C
          KEY_BLUE                 0x6897 # D
          KEY_UNKNOWN              0x14EB # Picture Mode
          KEY_UNKNOWN              0xD42B # Sound Mode
          KEY_UNKNOWN              0x00FF # Dual I-II
          KEY_UNKNOWN              0xE41B # AD
          KEY_UNKNOWN              0x7C83 # Picture Size
          KEY_SUBTITLE             0xA45B # Subtitle
          KEY_REWIND               0xA25D # <<
          KEY_PAUSE                0x52AD # ||
          KEY_FORWARD              0x12ED # >>
          KEY_RECORD               0x926D
          KEY_PLAY                 0xE21D
          KEY_STOP                 0x629D
      end codes

end remote
~~~

En esta web hay algunas configuraciones de mandos: http://lirc.sourceforge.net/remotes/

El fichero de configuración lo copiamos en el directorio /etc/lirc/lircd.conf.d/

En el fichero nos fijamos en el parámetro *name*, podemos cambiarlo si queremos, podemos guardar varios ficheros de configuración de mandos diferentes y con ese nombre haremos referencia a un fichero de configuración o mando concreto a la hora de enviar señales.

Reiniciamos el servicio para cargar la nueva configuración:  
`sudo /etc/init.d/lircd stop`  
`sudo /etc/init.d/lircd start`

##### Funcionamiento:

Listar todos las teclas grabadas del mando (*tv* es el nombre de mi mando, ver fichero de configuración):  
`irsend LIST tv ""`

Enviar pulsación botón de encendido una vez:  
`irsend SEND_ONCE tv KEY_POWER`

*Si no hemos encontrado la configuración de nuestro mando, necesitaremos crearla grabando las señales que emite. Para ello necesitaremos añadir un diodo receptor y seguir los pasos que se describen a continuación.*

### Receptor

###### Materiales:

- Diodo LED receptor de IR de tres pines

Existen diodos receptores de 2 patillas pero no son adecuados para este trabajo.

![Conexiones led receptor IR](Imagenes/ReceptorIR.png)

Conectar el led receptor como se indica en el dibujo.

Comprobar funcionamiento:  
`sudo /etc/init.d/lircd stop`  
`mode2 -d /dev/lirc0`  

(Si no funciona probar `mode2 -d /dev/lirc1`)

La consola quedará a la espera de una entrada. Entonces pulsa los botones del mando a distancia apuntando hacia el receptor y 
si funciona aparecerán palabras y números en la pantalla. Si no funciona no pasará nada.

Para grabar las señales que emite nuestro mando a distancia hay que ejecutar el siguiente comando y seguir sus instrucciones (en mi caso el dispositivo es /dev/lirc1, si no funciona prueba con /dev/lirc0):

`irrecord --disable-namespace -d /dev/lirc1`

Si has seguido las instrucciones y todo ha ido bien, habrás generado el fichero de configuración de tu mando. Por favor, compártelo 
por internet para que otros puedan utilizarlo. Coloca el fichero en el directorio correspondiente y pruébalo siguiendo los pasos descritos más arriba.

*Habrás observado que este mando no tiene botones, por tanto sólo se puede utilizar mediante comandos. Nos puede servir, por ejemplo, para controlar automáticamente un aire acondicionado programando límites de temperatura y añadiendo un sensor. 
En este caso quiero controlar la televisión por voz. Para ello hay que añadir algún software de reconocimiento de voz y programar las órdenes, esto lo dejo para otro manual.*

*Fuentes (¡Cuidado!: están desactualizadas y hay algunos errores):*

https://www.hackster.io/austin-stanton/creating-a-raspberry-pi-universal-remote-with-lirc-2fd581

https://www.instructables.com/id/Raspberry-Pi-Zero-Universal-Remote/