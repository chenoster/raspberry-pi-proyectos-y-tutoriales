## Apagado seguro: pon un botón de apagado/reinicio a tu Raspberry pi

![rpi zero con botón de apagado](Imagenes/rpizero_boton.png)

### Materiales:

- Raspberry Pi Zero  
- Pulsador normalmente abierto (sólo hace contacto mientras se pulsa el botón)  
- Soldador de estaño  

### Funcionamiento:

- Reiniciar: Mantener pulsado el botón de 2 a 5 segundos  
- Apagar: Mantener pulsado el botón más de 5 segundos  

### Instalación:

#### Hardware:

1) El pulsador tiene 4 patas, hay que identificar dos que no estén unidas, es decir, que no pase la electricidad mientras no pulsemos el botón. Dejamos esas dos patas y quitamos las otras dos para que no molesten ni puedan hacer contactos indesados.

![Pulsador](Imagenes/pulsador.png)

2) Soldamos el pulsador entre la GPIO27 y GND. Esto es para la Raspberry pi Zero, para otros modelos consultar la web de referencia (al final de este texto). 

![Soldadura](Imagenes/rpizero_boton_soldadura.png)

#### Software:

Lanzar la siguiente secuencia de comandos:

wget https://github.com/scruss/shutdown_button/archive/master.zip  
unzip master.zip  
cd shutdown_button-master  
sudo apt install python3-gpiozero  
sudo mkdir -p /usr/local/bin  
chmod +x shutdown_button.py  
sudo cp shutdown_button.py /usr/local/bin  
sudo cp shutdown_button.service /etc/systemd/system  
sudo systemctl enable shutdown_button.service  
sudo systemctl start shutdown_button.service  


Reiniciar y probar. 


Fuente: https://scruss.com/blog/2017/10/21/combined-restart-shutdown-button-for-raspberry-pi/