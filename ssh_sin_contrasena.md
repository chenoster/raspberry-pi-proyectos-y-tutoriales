## Conecta por SSH de manera segura sin que escribir la contraseña

Si no quieres escribir la contraseña cada vez que conectas a tu rpi, o quieres utilizar un script para automatizar ciertas tareas y que la conexión siga siendo segura, puedes hacerlo con un par de claves pública/privada.

Para ello hay que seguir dos sencillos pasos (todo se hace en el pc con el que vamos a conectar a la rpi):

1) Generar un par de claves.

`ssh-keygen -b 4096 -t rsa`

Pulsamos enter para aceptar los valores por defecto.

2) Ahora le pasamos la clave pública a la raspberry con el siguiente comando:

`ssh-copy-id usuario@ip_rpi`

(Si es para un script que requiere permisos de administrador habrá que pasarle la clave pública al usuario root: ssh-copy-id root@ip_rpi)


La próxima vez que nos conectemos a la rpi no nos pedirá la contraseña. 

*Gracias a Paco por la idea.*