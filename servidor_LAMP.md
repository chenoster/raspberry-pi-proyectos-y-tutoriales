## Instalar servidor Web LAMP (Linux, Apache, MariaDB y PHP)

1) Partimos de una instalación limpia y actualizada de Ubuntu Server

2) Instalamos el servidor Web apache y el navegador en modo consola links:  
`sudo apt-get install apache2 links`

3) Comprobamos que el servicio Web está activo:  
`systemctl status apache2`  

La instalación habilita y levanta el servicio.

4) Vamos a comprobar que el servidor está levantado y accesible:  
`links http://localhost`  
(Salir pulsando q o ctrl+c)

5) Instalamos el servidor de base de datos:  
`sudo apt-get install mariadb-server`

6) Securizamos el gestor de base de datos con el siguiente script:  
`sudo mysql_secure_installation`

7) Accedemos a la base de datos:  
`sudo mysql -u root -p`  
(exit para salir)

8) Instalamos PHP:  
`sudo apt-get install php php-mysql`

9) Reiniciamos los servicios:  
`systemctl restart apache2`  
`systemctl restart mariadb`

10) Comprobamos que funciona apache+php, primero creamos una sencilla página php:  
`sudo nano /var/www/html/info.php`  
El contenido del fichero debe ser el siguiente:
~~~
<?php
phpinfo();
?>
~~~
Y accedemos desde el navegador:

`links http://localhost/info.php`


Fuente: https://jugandoaseringeniero.wordpress.com/2020/07/08/instalar-un-servidor-lamp-ubuntu-server-20-04/