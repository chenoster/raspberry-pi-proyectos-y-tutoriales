## Instalación de Chamilo, plataforma de e-learning, buenísima alternativa a Moodle

1) Partimos del servidor LAMP instalado sobre Ubuntu Server [ver tutorial instalación](https://gitlab.com/chenoster/raspberry-pi-proyectos-y-tutoriales/-/blob/master/servidor_LAMP.md)

2) Instalamos paquetes necesarios:  
`sudo apt install apache2 libapache2-mod-php php-pear php-dev php-gd php-curl php-intl php-mysql php-mbstring php-zip php-xml php-cli php-apcu unzip`  
(Puede que algunos de estos paquetes ya estén instalados)

3) Primero tenemos que crear una base de datos y un usuario:  
`sudo mysql -u root -p`  
`CREATE DATABASE chamilo;`  
`use chamilo;`  
`CREATE USER 'chamilo'@'localhost' IDENTIFIED BY 'chamilo123';`  
`GRANT ALL PRIVILEGES ON chamilo.* TO 'chamilo'@'localhost';`  
`exit`

4) Descargamos Chamilo en la rpi, esta URL la podemos encontrar en la página oficial:  
`wget https://github.com/chamilo/chamilo-lms/releases/download/v1.11.12/chamilo-1.11.12-php7.2.zip`  
`unzip chamilo-1.11.12-php7.2.zip`  
`mv chamilo-1.11.12 chamilo`  
`sudo cp -R chamilo /var/www/`  
`cd /var/www`  
`sudo chown -R www-data:www-data chamilo`  
(El directorio /var/www/html/ lo podemos borrar si no lo usamos)

5) Configurar redirecciones en apache:  
`sudo a2enmod rewrite`  

El fichero de configuración se encuentra en el directorio: */etc/apache2/sites-enabled/*, hay que incluir el siguiente bloque:
~~~
<Directory />
  AllowOverride All
  Require all granted
</Directory>
<Directory /var/www/chamilo/>
  AllowOverride All
  Require all granted
</Directory>
~~~
(Revisar la configuración, cambiar las rutas donde sea necesario)

`systemctl restart apache2`

6) Cambiar parámetros de php (/etc/php/7.4/apache2/php.ini):
~~~
session.cookie_httponly = 1
upload_max_filesize = 50M
post_max_size = 300M
~~~

7) Desde tu ordenador accede a la siguiente URL: 

http://ip_raspberry

Seguir los pasos de instalación.

8) Últimas modificaciones de seguridad: 

`sudo chmod -R 0555 /var/www/aulavirtual/app/config/`  
`sudo rm -R /var/www/aulavirtual/main/install/`  

*Fuentes:*   
https://11.chamilo.org/documentation/installation_guide_es_ES.html  
https://www.evaristogz.com/despliegue-instalacion-chamilo-elearning/

---

### Copia de seguridad de Chamilo
Para hacer una copia de seguridad completa, por un lado hay que exportar la base de datos a un fichero y además copiar toda la carpeta /var/www/chamilo.

#### Exportar base de datos:
Exportamos a un fichero que luego tenemos que guardar en algún dispositivo seguro:

`mysqldump -u username -p database_name > data-dump-$(date +%d-%m-%Y).sql`

Para recuperar la base de datos a partir de fichero:

`mysql -u username -p new_database < data-dump.sql`  
(Primero hay que crear la base de datos con el mismo nombre si no existe)

#### Crear una copia de todos los archivos, comprimir y guardar en un dispositivo seguro:

`tar -zcvpf /backup/chamilo-backup-$(date +%d-%m-%Y).tar.gz /var/www/chamilo`

Un paso adicional muy recomentable cuando descargamos un fichero, es verificar la integridad de la descarga.  
Con el comando `md5sum nombre_fichero`, aplicado sobre el original y la copia, nos debe devolver el mismo resultado en ambos casos.

*Fuentes:*
https://www.digitalocean.com/community/tutorials/how-to-import-and-export-databases-in-mysql-or-mariadb

