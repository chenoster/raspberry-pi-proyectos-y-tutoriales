### Accede a tu Raspberry Pi por ssh conectándola al puerto USB de tu ordenador:

Así podrás manejar tu rpi sin teclado, monitor, cable de red, ni wifi.

1) Partimos de la tarjeta microSD con el sistema operativo instalado. Vamos a modificar algunos archivos desde nuestro ordenador.
2) Navegamos hasta la partición boot de la tarjeta y añadimos la siguiente linea en el fichero config.txt:
    `dtoverlay=dwc2`
3) En el mismo directorio creamos un archivo vacío que se llame *ssh*
4) Editamos el fichero cmdline.txt y añadimos el parámetro `modules-load=dwc2,g_ether` justo despues de rootwait

Ahora colocamos la tarjeta en nuestra raspberry y la conectamos al puerto USB del ordenador.

Creamos una nueva conexión cableada con IPV4 como "Enlace local" y IPV6 "Ignorar", nos conectamos a la nueva red y ya tendremos acceso por ssh (si usamos Linux):

`ssh pi@raspberrypi.local` 
(Raspbian y rpi OS: usuario=pi, contraseña=raspberry)

Desde Windows podemos acceder con la aplicación putty.

** Referencia: https://blog.donnikitos.com/access-the-raspberry-pi-via-usb-and-ssh-otg/

#### Para compartir internet con la Raspberry

Si hemos seguido los pasos anteriores y ya tenemos el control de la rpi por ssh, notaremos que no tiene acceso a internet.
Tenemos que compartir la conexión de nuestro ordenador con la rpi:

1) En la configuración de la red, en IPV4, en lugar de "Enlace local" vamos a seleccionar "Compartida con otros equipos".  
2) Reiniciamos la rpi.  
3) Miramos la ip que tenemos en la red: `ip a` (p.e. 10.42.0.1/24)  
4) Buscamos la rpi en la red: `sudo nmap -sn 10.42.0.0/24` (Siguiendo el ejemplo)
5) Accedemos con ssh por ip: `ssh pi@10.42.0.4`


** Referencia para Windows: https://notenoughtech.com/featured/raspberry-pi-zero-ssh-usb/  
*** *Probado con Raspberry Pi Zero W + Dongle USB. Sistema operativo: Raspberry Pi OS (Antiguo Raspbian, basado en Debian 10).*
