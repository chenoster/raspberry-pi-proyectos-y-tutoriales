### Instala el Sistema Operativo Oficial de Raspberry Pi

Raspberry Pi OS (antes llamado Raspbian), es el sistema operativo oficial para rpi basado en Debian. 

https://www.raspberrypi.org/downloads/raspberry-pi-os/

Para grabarlo en la tarjeta microSD existen diferentes opciones, pero la más sencilla es utilizar la herramienta balenaEtcher:

https://www.balena.io/etcher/