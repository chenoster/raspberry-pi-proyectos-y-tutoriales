## Conectar diodos LED y controlarlos por programación

![rpi zero con leds soldados a la placa](Imagenes/rpi_led.png)

Se pueden conectar diodos led directamente a la rpi, la patilla larga (ánodo) va siempre al polo positivo, en este caso el polo positivo es cualquiera de los pines GPIO. La pata corta (cátodo) va a cualquier pin tierra (GROUNG o GND) que es el polo negativo. 

Si tenemos un diodo con las patillas iguales porque lo hemos reciclado de algún aparato y se han recortado  las patillas, nos fijamos en su interior: la parte más gruesa corresponde al polo negativo y el lado con la parte fina es el positivo.

Para ver la descripción de los pines recomiendo la siguiente página:

https://es.pinout.xyz/#


#### Instalar los siguientes paquetes:

`sudo apt-get install python3-gpiozero python-gpiozero`


En mi caso he conectado un led verde con su patilla larga al GPIO 5 (pin físico 29) y la patilla corta a tierra (pin 30). 
También he conectado un led rojo con su patilla larga al GPIO 6 (pin 31) y la patilla corta a tierra (pin 34)

Ahora creamos un pequeño script en python (prueba_led.py):

~~~
from gpiozero import LED
from time import sleep

ledv = LED(5) # Led verde en GPIO5
ledr = LED(6) # Led rojo en GPIO6

while True:
    ledv.on()
    ledr.off()
    sleep(3)
    ledv.off()
    ledr.on()
    sleep(3)

~~~
    
Guardamos el script y lo ejecutamos para probarlo:

python prueba_led.py